﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;


//---Simple TSConductor example using .Net.Sockets and WindowsForms
//---Includes bad practices and no exception handling but should work as a example. Please do better in your app. ;)

namespace TSConductorExample
{
    public partial class FormExample : Form
    {
        //---Constants
        //---Set Server IP
        const string SERVER_IP = "127.0.0.1";
        //---Set Port Number, default 47811
        const int PORT_NO = 47811;
        //---var to know if loco is being driven
        public static int LOCO_ACTIVE = 0;
        //--TCP Server object
        protected TcpClient tclient = new TcpClient();

        public FormExample()
        {
            InitializeComponent();
        }

        private void FormExample_Load(object sender, EventArgs e)
        {
            //---Reset the form
            Resetform();
        }

        private void FormExample_Shown(object sender, EventArgs e)
        {
            try
            {
                tclient.Connect(SERVER_IP, PORT_NO);
            }
            catch (Exception)
            {
                //--- close application if TSC server is not reachable
                Application.Exit();
            }

            TimerUpdate.Interval = 500;
            TimerUpdate.Enabled = true;
        }

        //---Very basic TCP functionality. Should work for this example.
        //---There are tons of TCPClient and Server examples available for every language and device if you don't know how to do better.
        private string sendData(string data, TcpClient client)
        {
            //--Our result String
            String result = "";

            //---add <END> tag to our data so we don't have to every time we call the function
            data = data + "<END>";
            //---data to send to the server
            string textToSend = data;

            NetworkStream nwStream = client.GetStream();
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

            //---send the data
            LabelInfo.Text = "Sending : " + textToSend;
            nwStream.Write(bytesToSend, 0, bytesToSend.Length);

            //---read the answer
            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            //--set result
            result = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
            LabelInfo.Text = "Received : " + result;

            //---return result
            return result;
        }

        //---Same as above but we don't expect an answer from the server
        private string sendDataNoResponse(string data, TcpClient client)
        {
            //--Our result String
            String result = "";

            //---add <END> tag to our data so we don't have to every time we call the function
            data = data + "<END>";
            //---data to send to the server
            string textToSend = data;

            NetworkStream nwStream = client.GetStream();
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

            //---send the data
            LabelInfo.Text = "Sending : " + textToSend;
            nwStream.Write(bytesToSend, 0, bytesToSend.Length);

            return result;
        }


        //---using the WinForms Timer for simplification 
        private void TimerUpdate_Tick(object sender, EventArgs e)
        {
            //---check if and which Loco is driven
            String LocoName = sendData("GetLocoName()",tclient); 
            if (LocoName.Length > 0)
            {
                //---check which Loco is driven
                //---we only want the BR101 from Munich to Augsburg in this example
                //---you probably have to do this in your app too since every loco has different controller
                //---best practice would be a template system, created using any of the GetControllerList functions
                if (LocoName == "RSC.:.MunichAugsburg.:.MA DB Baureihe 101 Engine")
                {
                    //Loco is being driven, check if first time since loco change
                    if (LOCO_ACTIVE == 0)
                    {
                        //initialize ui values
                        InitializeLocoForm(LocoName);
                    }

                    //--- we want to get the current Sifa status and the current speed
                    String SifaStatus = sendData("GetControllerValue(48;0)",tclient);
                    String Speed = sendData("GetControllerValue(18;0)",tclient);
                    //---remove unnecessary numbers decimal places
                    TextBoxSpeed.Text = double.Parse(Speed).ToString("N1");
                    //---alert the sifa button if it has to be pressed
                    if (SifaStatus == "1")
                    {
                        ButtonSifa.BackColor = Color.Red;
                        ButtonSifa.ForeColor = Color.White;
                    } else
                    {
                        ButtonSifa.BackColor = SystemColors.Control;
                        ButtonSifa.ForeColor = SystemColors.ControlText;
                    }
                }
            } else
            {
                //---no loco is being driven, reset form
                LabelInfo.Text = "No Loco active";
                Resetform();     
            }
        }

        //---function to reset the form
        private void Resetform()
        {
            TextBoxSpeed.Text = "0";
            TrackBarPower.Minimum = 0;
            TrackBarPower.Maximum = 1;
            TrackBarBreak.Minimum = 0;
            TrackBarBreak.Maximum = 1;
            TrackBarDirection.Minimum = 0;
            TrackBarDirection.Maximum = 2;
            ButtonSifa.BackColor = SystemColors.Control;
            ButtonSifa.ForeColor = SystemColors.ControlText;
            LOCO_ACTIVE = 0;
        }

        //---function to set values for the UI
        //---could be unnecessary if you have a good template system
        private void InitializeLocoForm(string LocoName)
        {
            LOCO_ACTIVE = 1;
            LabelInfo.Text = "Setting UI values";

            //should usually check for LocoName for template system for example
            int min = 0;
            int max = 1;
            int.TryParse(sendData("GetControllerValue(20;1)",tclient), out min);
            int.TryParse(sendData("GetControllerValue(20;2)",tclient), out max);
            // x10 because the UI component only allows us to use integer
            TrackBarPower.Minimum = min * 10;
            TrackBarPower.Maximum = max * 10;
            int.TryParse(sendData("GetControllerValue(26;1)",tclient), out min);
            int.TryParse(sendData("GetControllerValue(26;2)",tclient), out max);
            TrackBarBreak.Minimum = min * 10;
            TrackBarBreak.Maximum = max * 10;
            int.TryParse(sendData("GetControllerValue(23;1)",tclient), out min);
            int.TryParse(sendData("GetControllerValue(23;2)",tclient), out max);
            TrackBarDirection.Minimum = min;
            TrackBarDirection.Maximum = max;
        }

        private void ButtonSifa_Click(object sender, EventArgs e)
        {
            //---send controller button press
            sendDataNoResponse("SetControllerValue(46;1)",tclient);
            //---release sifa button
            sendDataNoResponse("SetControllerValue(46;0)", tclient);
        }


        //---functions to set labels which show the trackbar values and to send data to the TS
        //---should -map values via loconame to "real" values for labels
        private void TrackBarPower_Scroll(object sender, EventArgs e)
        {
            LabelPower.Text = TrackBarPower.Value.ToString();
            sendDataNoResponse("SetControllerValue(20;" + System.Convert.ToString(System.Convert.ToDecimal(TrackBarPower.Value) / System.Convert.ToDecimal(10)).Replace(".", ",") + ")",tclient);
        }
        private void TrackBarBreak_Scroll(object sender, EventArgs e)
        {
            LabelBreak.Text = TrackBarBreak.Value.ToString();
            sendDataNoResponse("SetControllerValue(26;" + System.Convert.ToString(System.Convert.ToDecimal(TrackBarBreak.Value) / System.Convert.ToDecimal(10)).Replace(".",",") + ")", tclient);
        }
        private void TrackBarDirection_Scroll(object sender, EventArgs e)
        {
            LabelDirection.Text = TrackBarDirection.Value.ToString();
            sendDataNoResponse("SetControllerValue(23;" + TrackBarDirection.Value + ")", tclient);
        }
    }
}
