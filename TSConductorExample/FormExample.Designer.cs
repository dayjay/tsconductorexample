﻿namespace TSConductorExample
{
    partial class FormExample
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextBoxSpeed = new System.Windows.Forms.TextBox();
            this.ButtonSifa = new System.Windows.Forms.Button();
            this.TrackBarPower = new System.Windows.Forms.TrackBar();
            this.TrackBarBreak = new System.Windows.Forms.TrackBar();
            this.TrackBarDirection = new System.Windows.Forms.TrackBar();
            this.LabelInfo = new System.Windows.Forms.Label();
            this.TimerUpdate = new System.Windows.Forms.Timer(this.components);
            this.LabelPower = new System.Windows.Forms.Label();
            this.LabelBreak = new System.Windows.Forms.Label();
            this.LabelDirection = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarBreak)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDirection)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxSpeed
            // 
            this.TextBoxSpeed.Enabled = false;
            this.TextBoxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxSpeed.Location = new System.Drawing.Point(265, 12);
            this.TextBoxSpeed.Name = "TextBoxSpeed";
            this.TextBoxSpeed.Size = new System.Drawing.Size(200, 68);
            this.TextBoxSpeed.TabIndex = 0;
            this.TextBoxSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ButtonSifa
            // 
            this.ButtonSifa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSifa.Location = new System.Drawing.Point(12, 221);
            this.ButtonSifa.Name = "ButtonSifa";
            this.ButtonSifa.Size = new System.Drawing.Size(693, 67);
            this.ButtonSifa.TabIndex = 1;
            this.ButtonSifa.Text = "Sifa";
            this.ButtonSifa.UseVisualStyleBackColor = true;
            this.ButtonSifa.Click += new System.EventHandler(this.ButtonSifa_Click);
            // 
            // TrackBarPower
            // 
            this.TrackBarPower.Location = new System.Drawing.Point(12, 12);
            this.TrackBarPower.Name = "TrackBarPower";
            this.TrackBarPower.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrackBarPower.Size = new System.Drawing.Size(42, 178);
            this.TrackBarPower.TabIndex = 2;
            this.TrackBarPower.Scroll += new System.EventHandler(this.TrackBarPower_Scroll);
            // 
            // TrackBarBreak
            // 
            this.TrackBarBreak.Location = new System.Drawing.Point(135, 12);
            this.TrackBarBreak.Name = "TrackBarBreak";
            this.TrackBarBreak.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrackBarBreak.Size = new System.Drawing.Size(42, 178);
            this.TrackBarBreak.TabIndex = 3;
            this.TrackBarBreak.Scroll += new System.EventHandler(this.TrackBarBreak_Scroll);
            // 
            // TrackBarDirection
            // 
            this.TrackBarDirection.Location = new System.Drawing.Point(663, 12);
            this.TrackBarDirection.Name = "TrackBarDirection";
            this.TrackBarDirection.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrackBarDirection.Size = new System.Drawing.Size(42, 178);
            this.TrackBarDirection.TabIndex = 4;
            this.TrackBarDirection.Scroll += new System.EventHandler(this.TrackBarDirection_Scroll);
            // 
            // LabelInfo
            // 
            this.LabelInfo.AutoSize = true;
            this.LabelInfo.Location = new System.Drawing.Point(13, 295);
            this.LabelInfo.Name = "LabelInfo";
            this.LabelInfo.Size = new System.Drawing.Size(41, 13);
            this.LabelInfo.TabIndex = 5;
            this.LabelInfo.Text = "Started";
            // 
            // TimerUpdate
            // 
            this.TimerUpdate.Interval = 500;
            this.TimerUpdate.Tick += new System.EventHandler(this.TimerUpdate_Tick);
            // 
            // LabelPower
            // 
            this.LabelPower.AutoSize = true;
            this.LabelPower.Location = new System.Drawing.Point(22, 193);
            this.LabelPower.Name = "LabelPower";
            this.LabelPower.Size = new System.Drawing.Size(13, 13);
            this.LabelPower.TabIndex = 6;
            this.LabelPower.Text = "0";
            // 
            // LabelBreak
            // 
            this.LabelBreak.AutoSize = true;
            this.LabelBreak.Location = new System.Drawing.Point(142, 193);
            this.LabelBreak.Name = "LabelBreak";
            this.LabelBreak.Size = new System.Drawing.Size(13, 13);
            this.LabelBreak.TabIndex = 7;
            this.LabelBreak.Text = "0";
            // 
            // LabelDirection
            // 
            this.LabelDirection.AutoSize = true;
            this.LabelDirection.Location = new System.Drawing.Point(669, 193);
            this.LabelDirection.Name = "LabelDirection";
            this.LabelDirection.Size = new System.Drawing.Size(13, 13);
            this.LabelDirection.TabIndex = 8;
            this.LabelDirection.Text = "0";
            // 
            // FormExample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 318);
            this.Controls.Add(this.LabelDirection);
            this.Controls.Add(this.LabelBreak);
            this.Controls.Add(this.LabelPower);
            this.Controls.Add(this.LabelInfo);
            this.Controls.Add(this.TrackBarDirection);
            this.Controls.Add(this.TrackBarBreak);
            this.Controls.Add(this.TrackBarPower);
            this.Controls.Add(this.ButtonSifa);
            this.Controls.Add(this.TextBoxSpeed);
            this.Name = "FormExample";
            this.Text = "TSConductorExample";
            this.Load += new System.EventHandler(this.FormExample_Load);
            this.Shown += new System.EventHandler(this.FormExample_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarBreak)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDirection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxSpeed;
        private System.Windows.Forms.Button ButtonSifa;
        private System.Windows.Forms.TrackBar TrackBarPower;
        private System.Windows.Forms.TrackBar TrackBarBreak;
        private System.Windows.Forms.TrackBar TrackBarDirection;
        private System.Windows.Forms.Label LabelInfo;
        private System.Windows.Forms.Timer TimerUpdate;
        private System.Windows.Forms.Label LabelPower;
        private System.Windows.Forms.Label LabelBreak;
        private System.Windows.Forms.Label LabelDirection;
    }
}

